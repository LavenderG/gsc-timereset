#ifndef GUARD_MONEY_CALC_H
#define GUARD_MONEY_CALC_H

/**
 * Money calculation constants and
 * function declarations.
 **/

/**
 * MONEY_INVALID_NUMBER: number used for invalid money value calculation.
 * MONEY_MAX_VALUE: maximum money.
 **/
#define MONEY_INVALID_NUMBER 0xFFFF
#define MONEY_MAX_VALUE      999999

// Money bitmasks
#define MONEY_FIRST_BYTE_MASK  0x00FF0000
#define MONEY_SECOND_BYTE_MASK 0x0000FF00
#define MONEY_THIRD_BYTE_MASK  0x000000FF

/**
 * Calculates the money number for a given money quantity.
 * money: the money used in the number calculation.
 * return: the money number or MONEY_INVALID_NUMBER if the money
 *      quantity is not valid.
 **/
unsigned short calculate_money_number(unsigned int money);

#endif /* ifndef GUARD_MONEY_CALC_H */

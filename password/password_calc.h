#ifndef GUARD_PASSWORD_CALC_H
#define GUARD_PASSWORD_CALC_H

/**
 * Password calculation constants and
 * function declarations.
 **/

/**
 * Calculates the time password for a given name, money and TID.
 * name_number: the name number of the character.
 * money_number: the money number of the character.
 * tid_number: the tid number of the character.
 * return: the password number for the given input.
 **/
unsigned short calculate_password(unsigned short name_number,
  unsigned short                                 money_number,
  unsigned short                                 tid_number);

#endif

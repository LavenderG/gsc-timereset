#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "gsc_timereset.h"
#include "money/money_calc.h"
#include "name/name_calc.h"
#include "password/password_calc.h"
#include "tid/tid_calc.h"

int main(int argc, char * argv[])
{
    return gsc_timereset();
}

int gsc_timereset()
{
    print_help();

    // Ask trainer name
    char * trainer_input = ask_input("Input your trainer name: ");
    unsigned short trainer_name_number = calculate_name_number(trainer_input, strlen(trainer_input));
    free(trainer_input);
    while (trainer_name_number == NAME_INVALID_NUMBER) {
        trainer_input       = ask_input("Invalid trainer name. Please input a valid trainer name: ");
        trainer_name_number = calculate_name_number(trainer_input, strlen(trainer_input));
        free(trainer_input);
    }

    // Ask trainer money
    trainer_input = ask_input("Input your trainer money: ");
    char * end_ptr;
    unsigned long trainer_money         = strtoul(trainer_input, &end_ptr, 10);
    unsigned short trainer_money_number = calculate_money_number(trainer_money);
    free(trainer_input);
    while (end_ptr == NULL || trainer_money_number == MONEY_INVALID_NUMBER) {
        trainer_input        = ask_input("Invalid trainer money. Please input a valid money number: ");
        trainer_money        = strtoul(trainer_input, &end_ptr, 10);
        trainer_money_number = calculate_money_number(trainer_money);
        free(trainer_input);
    }

    // Ask trainer ID
    trainer_input = ask_input("Input your trainer ID: ");
    unsigned long trainer_id = strtoul(trainer_input, &end_ptr, 10);
    free(trainer_input);
    while (end_ptr == NULL || trainer_id > TID_MAX_VALUE) { // TODO: add return code to calculate_tid_number
        trainer_input = ask_input("Invalid trainer ID. Please input a valid TID: ");
        trainer_id    = strtoul(trainer_input, &end_ptr, 10);
        free(trainer_input);
    }
    unsigned short trainer_id_number = calculate_tid_number((unsigned short) trainer_id);

    unsigned short trainer_password = calculate_password(trainer_name_number, trainer_money_number,
        trainer_id_number);
    printf("Name number: %d, Money number: %d, TID number: %d\nPassword: %d\n",
      trainer_name_number, trainer_money_number, trainer_id_number, trainer_password);

    return EXIT_SUCCESS;
} /* gsc_timereset */

void print_help()
{
    printf("GSC Time Reset Password Calculator\n");
    printf("This program calculates the time reset password\n");
    printf("for a Western GSC game version.\n");
    printf("To access the reset clock menu do the following in the start screen:\n");
    printf("\t1. Press DOWN + SELECT + B\n");
    printf("\t2. While holding SELECT, let go of DOWN + B and press LEFT + UP\n");
    printf("\t3. Now let go of SELECT.\n");
    printf("To calculate the password you will need to know:\n");
    printf("\tYour trainer name, case sensitive (use @ for PK character and $ for MN character)\n");
    printf("\tYour trainer held money (not including Mom's saved money).\n");
    printf("\tYour trainer ID.\n\n");
}

char * ask_input(const char * input_prompt)
{
    char * input = read_input(input_prompt);

    if (input == NULL) {
        exit_mem_error();
        return NULL;
    } else {
        return input;
    }
}

void exit_mem_error()
{
    printf("Error: not enough memory\n");
    exit(GSC_TIMERESET_ERROR_MEM);
}

char * read_input(const char * input_prompt)
{
    unsigned int max = GSC_TIMERESET_DEFAULT_INPUT_SIZE;
    char * input     = malloc(max); /* allocate buffer */

    if (input == NULL) {
        return NULL;
    }

    printf("%s", input_prompt);

    while (1) { /* skip leading whitespace */
        int c = getchar();
        if (c == EOF) break;  /* end of file */
        if (!isspace(c)) {
            ungetc(c, stdin);
            break;
        }
    }

    unsigned int i = 0;
    while (1) {
        int c = getchar();
        if (isspace(c) || c == EOF) { /* at end, add terminating zero */
            input[i] = 0;
            break;
        }
        input[i] = c;
        if (i == max - 1) { /* buffer full */
            max  += max;
            input = (char *) realloc(input, max); /* get a new and larger buffer */
            if (input == NULL) {
                return NULL;
            }
        }
        i++;
    }

    return input;
} /* read_input */

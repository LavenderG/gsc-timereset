#include "charmap.h"
#include "name_calc.h"

unsigned short calculate_name_number(const char name[], unsigned int name_length)
{
    if (name_length > NAME_MAX_LENGTH) {
        return NAME_INVALID_NUMBER;
    }

    unsigned int used_length   = NAME_CALCULATION_LENGTH;
    unsigned short name_number = 0;

    if (name_length < NAME_CALCULATION_LENGTH) {
        used_length = name_length;
        // The termination string value is added when checking the name length
        name_number += CHAR_STRING_TERMINATION_VALUE;
    }

    unsigned int i;
    for (i = 0; i < used_length; i++) {
        unsigned char character_code;
        character_code = get_character_code(name[i]);

        if (character_code == CHAR_INVALID_VALUE) {
            return NAME_INVALID_NUMBER;
        }

        name_number += character_code;
    }

    return name_number;
}

unsigned char get_character_code(char name_character)
{
    switch (name_character) {
        case 'A':
            return CHAR_A_VALUE;

        case 'B':
            return CHAR_B_VALUE;

        case 'C':
            return CHAR_C_VALUE;

        case 'D':
            return CHAR_D_VALUE;

        case 'E':
            return CHAR_E_VALUE;

        case 'F':
            return CHAR_F_VALUE;

        case 'G':
            return CHAR_G_VALUE;

        case 'H':
            return CHAR_H_VALUE;

        case 'I':
            return CHAR_I_VALUE;

        case 'J':
            return CHAR_J_VALUE;

        case 'K':
            return CHAR_K_VALUE;

        case 'L':
            return CHAR_L_VALUE;

        case 'M':
            return CHAR_M_VALUE;

        case 'N':
            return CHAR_N_VALUE;

        case 'O':
            return CHAR_O_VALUE;

        case 'P':
            return CHAR_P_VALUE;

        case 'Q':
            return CHAR_Q_VALUE;

        case 'R':
            return CHAR_R_VALUE;

        case 'S':
            return CHAR_S_VALUE;

        case 'T':
            return CHAR_T_VALUE;

        case 'U':
            return CHAR_U_VALUE;

        case 'V':
            return CHAR_V_VALUE;

        case 'W':
            return CHAR_W_VALUE;

        case 'X':
            return CHAR_X_VALUE;

        case 'Y':
            return CHAR_Y_VALUE;

        case 'Z':
            return CHAR_Z_VALUE;

        case 'a':
            return CHAR_a_VALUE;

        case 'b':
            return CHAR_b_VALUE;

        case 'c':
            return CHAR_c_VALUE;

        case 'd':
            return CHAR_d_VALUE;

        case 'e':
            return CHAR_e_VALUE;

        case 'f':
            return CHAR_f_VALUE;

        case 'g':
            return CHAR_g_VALUE;

        case 'h':
            return CHAR_h_VALUE;

        case 'i':
            return CHAR_i_VALUE;

        case 'j':
            return CHAR_j_VALUE;

        case 'k':
            return CHAR_k_VALUE;

        case 'l':
            return CHAR_l_VALUE;

        case 'm':
            return CHAR_m_VALUE;

        case 'n':
            return CHAR_n_VALUE;

        case 'o':
            return CHAR_o_VALUE;

        case 'p':
            return CHAR_p_VALUE;

        case 'q':
            return CHAR_q_VALUE;

        case 'r':
            return CHAR_r_VALUE;

        case 's':
            return CHAR_s_VALUE;

        case 't':
            return CHAR_t_VALUE;

        case 'u':
            return CHAR_u_VALUE;

        case 'v':
            return CHAR_v_VALUE;

        case 'w':
            return CHAR_w_VALUE;

        case 'x':
            return CHAR_x_VALUE;

        case 'y':
            return CHAR_y_VALUE;

        case 'z':
            return CHAR_z_VALUE;

        case '(':
            return CHAR_OPENING_PARENTHESIS_VALUE;

        case ')':
            return CHAR_CLOSING_PARENTHESIS_VALUE;

        case ':':
            return CHAR_COLON_VALUE;

        case ';':
            return CHAR_SEMICOLON_VALUE;

        case '[':
            return CHAR_OPENING_BRACKET_VALUE;

        case ']':
            return CHAR_CLOSING_BRACKET_VALUE;

        case '-':
            return CHAR_DASH_VALUE;

        case '?':
            return CHAR_QUESTION_MARK_VALUE;

        case '!':
            return CHAR_EXCLAMATION_MARK_VALUE;

        case '.':
            return CHAR_DOT_VALUE;

        case '*':
            return CHAR_ASTERISK_VALUE;

        case '/':
            return CHAR_SLASH_VALUE;

        case ',':
            return CHAR_COMMA_VALUE;

        case '@':
            return CHAR_PK_VALUE; // PK character, represented as @

        case '#':
            return CHAR_MN_VALUE; // MN character, represented as #

        default:
            return CHAR_INVALID_VALUE;
    }
} /* get_character_code */

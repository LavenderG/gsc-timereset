#ifndef GUARD_TID_CALC_H
#define GUARD_TID_CALC_H

/**
 * TID calculation constants and
 * function declarations.
 **/

// TID constants
#define TID_MAX_VALUE 65535

// TID bitmasks
#define TID_FIRST_BYTE_MASK  0xFF00
#define TID_SECOND_BYTE_MASK 0x00FF

/**
 * Calculates the number for a given TID.
 * tid: the Trainer ID used in the calculation.
 * return: the TID number value.
 **/
unsigned short calculate_tid_number(unsigned short tid);

#endif /* ifndef GUARD_TID_CALC_H */

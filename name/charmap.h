#ifndef GUARD_CHARMAP_H
#define GUARD_CHARMAP_H

/*
 * Name character values
 */

// Uppercase letters
#define CHAR_A_VALUE 128
#define CHAR_B_VALUE 129
#define CHAR_C_VALUE 130
#define CHAR_D_VALUE 131
#define CHAR_E_VALUE 132
#define CHAR_F_VALUE 133
#define CHAR_G_VALUE 134
#define CHAR_H_VALUE 135
#define CHAR_I_VALUE 136
#define CHAR_J_VALUE 137
#define CHAR_K_VALUE 138
#define CHAR_L_VALUE 139
#define CHAR_M_VALUE 140
#define CHAR_N_VALUE 141
#define CHAR_O_VALUE 142
#define CHAR_P_VALUE 143
#define CHAR_Q_VALUE 144
#define CHAR_R_VALUE 145
#define CHAR_S_VALUE 146
#define CHAR_T_VALUE 147
#define CHAR_U_VALUE 148
#define CHAR_V_VALUE 149
#define CHAR_W_VALUE 150
#define CHAR_X_VALUE 151
#define CHAR_Y_VALUE 152
#define CHAR_Z_VALUE 153

// Lowercase letters
#define CHAR_a_VALUE 160
#define CHAR_b_VALUE 161
#define CHAR_c_VALUE 162
#define CHAR_d_VALUE 163
#define CHAR_e_VALUE 164
#define CHAR_f_VALUE 165
#define CHAR_g_VALUE 166
#define CHAR_h_VALUE 167
#define CHAR_i_VALUE 168
#define CHAR_j_VALUE 169
#define CHAR_k_VALUE 170
#define CHAR_l_VALUE 171
#define CHAR_m_VALUE 172
#define CHAR_n_VALUE 173
#define CHAR_o_VALUE 174
#define CHAR_p_VALUE 175
#define CHAR_q_VALUE 176
#define CHAR_r_VALUE 177
#define CHAR_s_VALUE 178
#define CHAR_t_VALUE 179
#define CHAR_u_VALUE 180
#define CHAR_v_VALUE 181
#define CHAR_w_VALUE 182
#define CHAR_x_VALUE 183
#define CHAR_y_VALUE 184
#define CHAR_z_VALUE 185

// Other characters
#define CHAR_OPENING_PARENTHESIS_VALUE 154 // (
#define CHAR_CLOSING_PARENTHESIS_VALUE 155 // )
#define CHAR_COLON_VALUE               156 // :
#define CHAR_SEMICOLON_VALUE           157 // ;
#define CHAR_OPENING_BRACKET_VALUE     158 // [
#define CHAR_CLOSING_BRACKET_VALUE     159 // ]
#define CHAR_DASH_VALUE                227 // -
#define CHAR_QUESTION_MARK_VALUE       230 // ?
#define CHAR_EXCLAMATION_MARK_VALUE    231 // !
#define CHAR_DOT_VALUE                 232 // .
#define CHAR_ASTERISK_VALUE            241 // *
#define CHAR_SLASH_VALUE               243 // /
#define CHAR_COMMA_VALUE               244 // ,

// Special characters
#define CHAR_PK_VALUE                 225 // @
#define CHAR_MN_VALUE                 226 // #
#define CHAR_STRING_TERMINATION_VALUE 80
#define CHAR_INVALID_VALUE            255

#endif /* ifndef GUARD_CHARMAP_H */

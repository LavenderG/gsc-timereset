#ifndef GUARD_NAME_CALC_H
#define GUARD_NAME_CALC_H

/**
 * Name calculation constants and
 * function declarations.
 **/

/**
 * NAME_MAX_LENGTH: name maximum length.
 * NAME_CALCULATION_LENGTH: number of characters used in the number calculation.
 * NAME_INVALID_NUMBER: returned when the name number cannot be calculated.
 **/
#define NAME_MAX_LENGTH         7
#define NAME_CALCULATION_LENGTH 5
#define NAME_INVALID_NUMBER     0xFFFF

/**
 * Gets the character code for a given character.
 * name_character: the input character.
 * return: the character code for a given character, or CHAR_INVALID_VALUE
 *      for invalid characters.
 **/
unsigned char get_character_code(char name_character);

/**
 * Calculates a name number for a given name. A name can contain only valid
 * characters and have a maximum length of NAME_MAX_LENGTH.
 * name: the name.
 * name_length: the length of name.
 * return: the calculated name number or NAME_INVALID_NUMBER for invalid names.
 **/
unsigned short calculate_name_number(const char name[], unsigned int name_length);

#endif /* ifndef GUARD_NAME_CALC_H */

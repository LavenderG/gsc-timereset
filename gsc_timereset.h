#ifndef GUARD_GSC_TIMERESET_H
#define GUARD_GSC_TIMERESET_H

/**
 * gsc_timereset constants and function declarations
 **/

// Memory error code
#define GSC_TIMERESET_ERROR_MEM          4
// Default read_input input size
#define GSC_TIMERESET_DEFAULT_INPUT_SIZE 20

/**
 * Main program
 * GSC Time reset
 **/
int gsc_timereset();

/**
 * Prints program help
 **/
void print_help();

/**
 * Exits program with mem error code
 **/
void exit_mem_error();

/**
 * Asks input with prompt input_prompt
 * input_prompt: the input prompt
 * return: the pointer to the allocated user input
 **/
char * ask_input(const char * input_prompt);

/**
 * Reads input from stdin
 * input_prompt: the input prompt
 * return: the pointer to the allocated user input, NULL if it cannot be allocated
 **/
char * read_input(const char * input_prompt);

#endif /* ifndef GUARD_GSC_TIMERESET_H */

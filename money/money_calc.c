#include "money_calc.h"

unsigned short calculate_money_number(unsigned int money)
{
    if (money > MONEY_MAX_VALUE) {
        return MONEY_INVALID_NUMBER;
    }

    unsigned char first_money_byte  = (money & MONEY_FIRST_BYTE_MASK) >> 16;
    unsigned char second_money_byte = (money & MONEY_SECOND_BYTE_MASK) >> 8;
    unsigned char third_money_byte  = money & MONEY_THIRD_BYTE_MASK;

    return first_money_byte + second_money_byte + third_money_byte;
}

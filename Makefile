OBJS = gsc_timereset.o tid/tid_calc.o password/password_calc.o name/name_calc.o money/money_calc.o
GSC_TIMERESET = gsc_timereset
HEADERS = .

CFLAGS = -Wall -Wextra -Wshadow -g
CC = gcc

all: ${GSC_TIMERESET}

${GSC_TIMERESET}: ${OBJS}
	${CC} ${CFLAGS} -o ${GSC_TIMERESET} ${OBJS}

%.o: %.c
	${CC} -c $< -o $@  -I ${HEADERS}

clean:
	rm -f ${OBJS} ${GSC_TIMERESET}

#include "tid_calc.h"

unsigned short calculate_tid_number(unsigned short tid)
{
    unsigned char first_tid_byte  = (tid & TID_FIRST_BYTE_MASK) >> 8;
    unsigned char second_tid_byte = tid & TID_SECOND_BYTE_MASK;

    return first_tid_byte + second_tid_byte;
}

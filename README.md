# gsc-timereset
Calculadora de la contraseña para reiniciar la fecha y hora en OPC.

## Compilar

Para compilar el programa, ejecutar:
```
make
```

## Uso
Para usar el programa, ejecutar:
```
./gsc_timereset
```


